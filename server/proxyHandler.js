import logger from "../utils/logger";
import { getDateTime } from "../src/helpers";
const request = require("request");

export function handleProxy(req, res, baseUrl) {
  const headers = {
    ["Content-Type"]: req.headers["content-type"]
  };
  if (req.headers["accept"]) {
    headers["accept"] = req.headers["accept"];
  }
  logger.info(
    "Info for API At " +
      getDateTime() +
      " => " +
      baseUrl +
      req.originalUrl +
      " and body " +
      JSON.stringify(req.body)
  );
  request(
    {
      url: baseUrl + req.originalUrl,
      method: req.method,
      headers: headers,
      body: req.body,
      json: true
    },
    function(error, response, body) {
      if (response && response.statusCode == 204) {
        const resBody = body ? body : {};
        resBody.message = resBody.message
          ? resBody.message
          : "No content found";
        res.status(200).send(resBody);
      } else if (response && response.statusCode < 350) {
        res.status(response.statusCode).send(body);
      } else {
        if (response && response.statusCode) {
          logger.error(
            "error-2 for API At " +
              getDateTime() +
              " => " +
              baseUrl +
              req.originalUrl +
              " and status code is " +
              response.statusCode
          );
          body.message = body.message ? body.message : "Something went wrong";
          res.status(response.statusCode).send(body);
        } else {
          logger.error(
            "error-2 for API At " +
              getDateTime() +
              " => " +
              baseUrl +
              req.originalUrl +
              " and status code is pending"
          );
          res.status(500).send({ message: "Getting no response from api" });
        }
      }
    }
  );
}