const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
import logger from "../utils/logger";
import { getDateTime } from "../src/helpers";
const cookieParser = require("cookie-parser");
import { handleProxy } from "./proxyHandler";
const app = express();
app.use(cookieParser());
app.use(bodyParser.json({ limit: "50mb" }));
app.use("/dist", express.static("dist"));
app.use(express.static("public"));

app.use("/api/*", function(req, res) {
  handleProxy(req, res, "https://intense-citadel-19789.herokuapp.com");
});

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "../public/index.html"), function(err) {
    if (err) {
      logger.error(
        "error-2 for API At " + getDateTime() + " => " + req.originalUrl
      );
      res.status(500).send(err);
    }
  });
});

process.on("unhandledRejection", function(err) {
  logger.error("UnhandledRejection error" + err);
});
process.on("uncaughtException", function(error) {
  logger.error("uncaughtException" + error);
  logger.error("Error Stack", error.stack);
});
app.listen(process.env.PORT || 4000, () =>
  console.log(`listening on port ${process.env.PORT || 4000}!`)
);
