import Home from "./containers/Home";
import AppDetails from "./containers/AppDetails";

export const routesPath = {
  HOME: "/",
  APP_DETAILS: "/appdetails"
};

export const routes = [
  {
    path: routesPath.HOME,
    exact: true,
    component: Home,
    customHeader: false
  },
  {
    path: routesPath.APP_DETAILS,
    exact: true,
    component: AppDetails,
    customHeader: false
  }
];
