import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import configureStore from "../configureStore";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

const { persistor, store } = configureStore();

const app = (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </MuiThemeProvider>
);

ReactDOM.render(app, document.getElementById("root"));
