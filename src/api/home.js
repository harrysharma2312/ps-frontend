import axios from "axios";

export function fetchAllData(page) {
  return axios({
    url: `/api/getAll?page=${page}`,
    method: "GET",
    headers: {
      "Content-Type": "Application/json"
    }
  })
    .then(res => res)
    .catch(err => err);
}

export function fetchAppData(appId) {
  return axios({
    url: `/api/getById?appId=${appId}`,
    method: "GET",
    headers: {
      "Content-Type": "Application/json"
    }
  })
    .then(res => res)
    .catch(err => err);
}
